<?php

function getTheLatestTimestamp($chatlogFilename)
{	
	$jsonString = file_get_contents($chatlogFilename);
	$chatlog = json_decode($jsonString);
	$latestMessage = end($chatlog);
	reset($latestMessage);
	return $latestMessage->timestamp;
}

date_default_timezone_set(date_default_timezone_get());
$chatlogFilename = "chatlog.json";
if($_POST['action'] == "send")
{
	$userID = $_POST['userID'];
	$msg = $_POST['msg'];
	$timestamp = $_POST['timestamp'];
	//$timestamp = new DateTime();
	$newJsonString = null;
	$content = array("userID" => $userID, "msg" => $msg, "timestamp" => $timestamp);
	if(!file_exists($chatlogFilename))
	{
		$newJsonString = json_encode(array($content));
	}
	else
	{
		$jsonString = file_get_contents($chatlogFilename);
		$chatlog = json_decode($jsonString);
		array_push($chatlog, $content);
		$newJsonString = json_encode($chatlog);
	}
	file_put_contents($chatlogFilename, $newJsonString);
	echo 200;
}
else if($_POST['action'] == "getAll")
	{
		$jsonString = file_get_contents($chatlogFilename);
		echo $jsonString;
	}
else if($_POST['action'] == "get")
	{
		$amount = $_POST['amount'];
		$jsonString = file_get_contents($chatlogFilename);
		$chatlog = json_decode($jsonString);
		$chatlogToBeSend = array();
		$count = count($chatlog);
		$i= $count-$amount>=0? $count-$amount : 0; //Set the start index of loop
		for(; $i<$count; $i++)
		{
			array_push($chatlogToBeSend, $chatlog[$i]);
		}
		$newJsonString = json_encode($chatlogToBeSend);
		echo $newJsonString;
	}
else if($_POST['action'] == "fetch")
	{
		$lastModifiedTime = $_POST['timestamp'];
		$currentModifiedTime = getTheLatestTimestamp($chatlogFilename);
		//$currentModifiedTime = filemtime($chatlogFilename);
		//$count = 0;
		while( $currentModifiedTime <= $lastModifiedTime)
		{
			usleep(10000);
			clearstatcache();
			$currentModifiedTime = getTheLatestTimestamp($chatlogFilename);
			//$count++;
			//if($count == 1000)break;
		}
		echo $currentModifiedTime;
	}
else if($_POST['action'] == "clear")
	{
		file_put_contents($chatlogFilename, "[]");
	}
?>